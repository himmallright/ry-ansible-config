## Archive Role

This role will tar up a single or list of multiple directories
(`archvie_dirs`), into a single tar package (`archive_path`).

## Requirements

- Server configured for ansible
    - SSH setup
    - Password-less `sudo` configured
    - If running on local machine, ansible installed

## Variables

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `archive_dirs` | The location of the directories to package up. Can be a single string or a list of multiple. | **Required**, must be provided |
| `archive_path` | The location to save the output package. | **Required**, must be provided |
| `archive_format` | The type of compression to use (`bz2`, `gz`, `tar`, `xz`, or `zip`) | Defaults to `gz` |
| `user` | The user to run everything under | Defaults to `ansible_user_id` |

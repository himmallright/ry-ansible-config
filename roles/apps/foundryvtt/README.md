## Foundry VTT role

This role will setup a server to run [foundryvtt](https://foundryvtt.com),
install it, create a systemd service file for it, and start the service.

## Requirements

- Server (Fedora) configured for ansible
    - SSH setup
    - Password-less `sudo` configured
    - If running on local machine, ansible installed
- The downloaded foudryvtt `.zip` package to provide the playbook

## Playbooks

There are two main functionalities of this role, and two defined playbooks to
operate those tasks.

### Provision Foundry

The First playbook, `playbooks/apps/provision_foundryvtt.yaml`, will setup a new Fedora server to host foundryvtt. It will install the dependencies, open the firewall port, and even configure a systemd system file to manage the foundry server.

It will also install and start the foundry server. This can be completed as a
freash install by providing the installer zip package and passing it through
with the `foundryvtt_install_package` variable.

Alternatively, a previously archived foundry package (see next section) can be provided to restore an archived foundry instance and data during provisioning. This is done by setting the `restore_foundryvtt_archive` variable to the desired archive package.

### Archiving

A foundry server can be archived using the second playbook, `playbooks/apps/provision_foundryvtt.yaml`. This playbook will take the `foundryvtt` and `foundrydata` folders (defined with the `foundryvtt_dir` and `foundrydata_dir` variables, respectively) on a remote machine, and package them into a time-stamped `gz` (default, change with `foundry_archive_type`) package. The package is built on the remote machine (at `remote_temp_package_dir`), and then downloaded to the machine running the playbook (at `foundry_archive_download_dir`). To delete the temporary package built on the server, make sure that `delete_temp_foundry_archive` is set to `True` (this is the default).

These packages can be provided when provisioning a new foundry server, in order to restore it.

## Variables

### General

These are general variables that are used in both provisioning and archiving
use-cases of the role. These should be provided for both playbooks.

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `user` | The user to run everything under | Defaults to `ansible_user_id` |
| `provision_foundry` | A boolean that when `True` runs the provisioning tasks. | Defaults to `True` |
| `foundryvtt_dir` | The directory to unpackage and install foundryvtt to | Defaults to `/home/{{ user }}/foundryvtt/` |
| `foundrydata_dir` | The directory to store foundryvtt data | Defaults to `/home/{{ user }}/foundrydata` |


### Provisioning

These are provisioning-specific variables. Note, that either
`foundry_install_package` *or* `restore_foundry_vtt_archive` **must** be
supplied in order to provision foundryvtt. If both are provided, the system
will use `restore_foundry_vtt_archive`.

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `foundrtyvtt_install_package` | The location on the machine running the playbook, of the downloaded foundryvtt zip package| Defaults to `/home/{{ user }}/foundryvtt.zip. Is over-ridden if `restore_foundryvtt_archive` is defined. |
| `restore_foundryvtt_archive` | A foundryvtt archive package, to load (restore from) during provisioning. | If provided, overrides `foundryvtt_install_package`. |
| `foundry_unarchive_dir` | The location to unpack and install the foundry packages on the remote server| Defaults to `/home/{{ user }}/` |
### Archiving 

These variables are specific to the foundry archiving tasks. The archive
tasks are run when the `foundry_archive_download_dir` is provided.

| Variable | Description | Default/Required |
|----------|-------------|---------------|
| `foundry_archive_download_dir` | The directory that the archive package will be downloaded to. | Defaults to `Null`, when provided, triggers archive tasks. |
| `foundry_archive_type` | Sets the type of package for the archive. | Defaults to `gz` |
| `remote_temp_package_dir` | The directory on the remote machine where the archive package will be assembled | Defaults to `/home/{{ user}}`
| `delete_temp_foundry_archive` | A boolean to delete the temporary archive on the remote machine after it is downloaded. | Defaults to `True` |
